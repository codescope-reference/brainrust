#[derive(PartialEq, Debug)]
pub enum Token {
    MoveRight,
    MoveLeft,
    Inc,
    Dec,
    Print,
    Read,
    OpenBracket,
    CloseBracket,
    Comment(String),
}
